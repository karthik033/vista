;*******************************************************************
;* PRINT LAYOUT TEMPLATE                        FILE: Ticket.txt   *
;* VERSION: 1.1                           FOR: Vista Sample File   *
;* PRINTER: Epson TM-T88                                           *
;*******************************************************************
;*                    WARNING!!!!!!                                *
;*                    -------------                                *
;*     Do NOT amend unless you know what you are doing             *
;*       Notes included in the bottome of the file                 *
;*******************************************************************
;-------------------------------------------------------------------
[Definitions]
;-------------------------------------------------------------------
{FF}               =012
{CR}               =013
{LF}               =010
{Cancel}           =024
{CRLF}             =013010

{Reset}            =027064
{PrintBuffer}      =027033000027100004010000
{CutPaper}         =027105
{RightAlign}       =027097002
{LeftAlign}        =027097000
{CenterAlign}      =027097001
{BackToTopOfPage}  =027012s
{DraftMode}        =027120048
{StandardMode}     =027083
{PageMode}         =047076

{DoubleHeight}     =027033048
{HalfWidth}        =027033016
{QuadrupleSize}    =027104002
{ReverseOn}        =029066001
{ReverseOff}       =029066000

;Epson TM-T90P Logo print command.  Uploaded by Colour Logo Utility
;{PrintLogo}        =029040076006000048069048048001001
;Epson TM-T80IIP / TM-T90P Logo print command. Uploaded by the TM Flash Logo Setup Utility
{PrintLogo}       =028112001051
;{PrintBarcode}    =029107002052057049050051052053054055056057048000
{PrintBarcode}     =029107004
{BarcodeHeight}    =029104075
{BarcodeWidth}     =029119002
{BarcodeHeight1}    =029104025
{BarcodeWidth1}     =029119002
{BarcodeNumbers}   =029072002
{Null}             =000

{EmphasizedModeOn}      =027069001
{EmphasizedModeOff}     =027069000
{SmoothModeOn}          =029098001
{SmoothModeOff}         =029098000
{DoubleStrikeModeOn}    =027071001
{DoubleStrikeModeOff}   =027071000
{UnderlineModeOnWidth1} =027045001
{UnderlineModeOnWidth2} =027045002
{UnderlineModeOff}      =027045000

;character size, use 029033(size)
;replace size with the horizontal adjustment plus the vertical adjustment
;normal is 000,
;vertical values are 000=1, 001=2, 002=3, 003=4, 004=5, 005=6, 006=7, 007=8
;horizontal values are 000=1, 016=2, 032=3, 048=4, 064=5, 080=6, 096=7, 112=8 
;vertical 1 horizontal 1
{Font0}                  =029033000
;vertical 2 horizontal 2
{Font1}                  =029033017
;vertical 3 horizontal 2
{Font2}                  =029033019
;vertical 3 horizontal 4
{Font3}                  =029033050
;vertical 3 horizontal 3
{Font4}                  =029033034
;vertical 2 horizontal 1
{Font5}                  =029033001
;vertical 1 horizontal 2
{Font6}                  =029033016

{BigFont}                =027033000
{SmallFont}              =027033001
{BigFont-Emp}            =027033008
{SmallFont-Emp}          =027033009
{BigFont-2H}             =027033016
{SmallFont-2H}           =027033017
{BigFont-2W}             =027033032
{SmallFont-2W}           =027033033
{BigFont-Underline}      =027033128
{SmallFont-Underline}    =027033129
{LineSpacing}            =027051050
{DefaultLineSpacing}     =027050000
;-------------------------------------------------------------------
[CalcFields]
;-------------------------------------------------------------------
;operators supported are + - / *, brackets are not supported
;{MyCalcField} = !TicketCost!+'100'
{NewValue} = !PRICETAX1!+'0.00'
{Location} = !SCREENNO!+'0'
{ItemCostInPackage} = !PackageCost!-!TicketCost!
{ItemCostInTax} = !TicketCost!+'0.00'

;-------------------------------------------------------------------
[Formats]
;-------------------------------------------------------------------
;these formats are for the barcode printing
{TransactionNumber}   =00000000
{SequenceNumber}      =000
{ScreenNo}            =#0 
{PRICEBEFORETAX}      =##000.00
{PRICETAX1}           =00.00
{PRICETAX2}           =00.00
{PRICETAX3}           =00.00
{PRICETAX4}           =00.00
{TicketCost}          =##000.00
{Cinema}              =PadRight(15)
{AdmitDetails}        =PadRight(20)
{TicketTypeDesc}      =PadLeft(12)
;{?MyCalcField?}      =####0.00
{?NewValue?} 	      = RIGHT(3) 
;----------------------------------------------------------------
[Header]
{Reset};
{StandardMode};

;----------------------------------------------------------------
;----------------------------------------------------------------
[Detail]
;----------------------------------------------------------------
{CRLF}{LineSpacing};
{CRLF}{CenterAlign}{BigFont-2H}{EmphasizedModeOn}{Font5}S2 Mallapur;
{CRLF}{CenterAlign}{SmallFont}{Font0}SPI Cinemas Pvt Ltd.,;
{CRLF}{CenterAlign}{SmallFont}{Font0}No-2/1/1,Mallapur,Nacharam;
{CRLF}{CenterAlign}{SmallFont}{Font0}Hyderabad-500076;
{CRLF}{CenterAlign}{SmallFont}{Font0}GSTCODE :36AABCC7343L1ZH;
{CRLF}{CenterAlign}{SmallFont}{Font0}SAC :998554;

{CRLF}{LeftAlign}    {BigFont-2H}{EmphasizedModeOn}{Font5}<ScreenName> at <IIF~?Location?~GT~'6'~'S2 Mallapur'~'S2 Mallapur'>  {EmphasizedModeOff};
;{CRLF}{LeftAlign}   {BigFont-2H}{EmphasizedModeOn}{Font5} <ScreenName>;{EmphasizedModeOff};
{CRLF}{LeftAlign}    {BigFont-2H}{Font5}<FILMTITLEFULL>(<CENSORRATING>);
{CRLF}{LeftAlign}    <SESSIONATTRIBUTES>{CRLF};
{CRLF}{LeftAlign}    {Font5}<SESSIONSTARTFULL>;
{CRLF}{LeftAlign}    {BigFont-2H}{Font5}<AREA> <RowID>-<SeatID>;
;{CRLF}{CenterAlign}{SmallFont}{EmphasizedModeOn}{Font0}(Nett Rate Includes TMC of Rs.3){EmphasizedModeOff};
{CRLF}{LeftAlign}     {SmallFont}{Font0}<TicketPayment> <TransactionNumber>/<SequenceNumber><TextTax><TaxNumber>;               
{CRLF}{RightAlign}{SmallFont}{Font0}Nett: Rs. <PRICEBEFORETAX>       ;
;{CRLF}{RightAlign}{SmallFont}{Font0}    LBET: Rs.  <PRICETAX1>       ;
{CRLF}{RightAlign}{SmallFont}{Font0}CGST(<IIF~?ItemCostInTax?~GT~'118.00'~'9%)'~'6%)'>: Rs.  <PRICETAX3>       ;
{CRLF}{RightAlign}{SmallFont}{Font0}SGST(<IIF~?ItemCostInTax?~GT~'118.00'~'9%)'~'6%)'>: Rs.  <PRICETAX4>       ;
{CRLF}{LeftAlign}{SmallFont}{EmphasizedModeOn}{Font0}                                 Total: Rs. <TicketCost>{EmphasizedModeOff};
{CRLF}{CenterAlign}{SmallFont}{Font6}<IIF~?ItemCostInPackage?~GT~'00.00'~'3D glass usage fee Rs.35'~''>  ;
{CRLF}{CenterAlign}{SmallFont}{Font0}<IIF~?ItemCostInPackage?~GT~'00.00'~IIF|?ItemCostInTax?|GT|'118.00'|'(Additional to ticket charge. Includes GST of 18%)'|'(Additional to ticket charge. Includes GST of 12%)'~''>;


;{CRLF}{CenterAlign}{SmallFont}---------------------------------------;
;{CRLF}{LeftAlign}  P {BigFont-2H}{EmphasizedModeOn}{Font5}<ScreenName> at <IIF~?Location?~GT~'6'~'escape'~'Sathyam Cinemas'> {SmoothModeOff};
;{CRLF}{LeftAlign}   {BigFont-2H}{EmphasizedModeOn}{Font5}  <ScreenName>{SmoothModeOff};
;{CRLF}{LeftAlign}    {BigFont-2H}{Font5}<FILMTITLEFULL>(<CENSORRATING>);
;{CRLF}{LeftAlign}    {BigFont-2H}<SESSIONATTRIBUTES>{CRLF};
;{CRLF}{LeftAlign}    {BigFont-2H}{Font5}{Font5}<SESSIONSTARTFULL>;
;{CRLF}{LeftAlign}    {BigFont-2H}{Font5}<AREA> <RowID>-<SeatID>;
;{CRLF}{CenterAlign}{SmallFont}{Font0}{EmphasizedModeOn}             (Nett Rate Includes TMC of Rs.1){EmphasizedModeOff};
;{CRLF}{LeftAlign}     {SmallFont}{Font0}<TicketPayment>;
;{CRLF}{RightAlign}{SmallFont}{Font0}Nett: Rs. <PRICEBEFORETAX>       ;
;{CRLF}{RightAlign}{SmallFont}{Font0}    LBET: Rs.  <PRICETAX1>       ;
;{CRLF}{RightAlign}{SmallFont}{Font0}CGST(<IIF~?ItemCostInTax?~GT~'118.00'~'14%)'~'9%)'>: Rs.  <PRICETAX3>       ;
;{CRLF}{RightAlign}{SmallFont}{Font0}SGST(<IIF~?ItemCostInTax?~GT~'118.00'~'14%)'~'9%)'>: Rs.  <PRICETAX4>       ;
;{CRLF}{LeftAlign}{SmallFont}{EmphasizedModeOn}{Font0}                                 Total: Rs. <TicketCost>{EmphasizedModeOff};
;{CRLF}{CenterAlign}{SmallFont}{Font6}<IIF~?ItemCostInPackage?~GT~'00.00'~'3D glass usage fee Rs.35'~''>;
;{CRLF}{CenterAlign}{SmallFont}{Font0}<IIF~?ItemCostInPackage?~GT~'0.00'~IIF|?ItemCostInTax?|GT|'118.00'|'(Additional to ticket charge includes GST of 28%)'|'(Additional to ticket charge includes GST of 18%)'~''>;


;{CRLF}<MESSAGESTUB1>;
;{CRLF}<MESSAGESTUB1ALT>;
;{CRLF}<MESSAGESTUB2>;
;{CRLF}<MESSAGESTUB2ALT>;



{CRLF}{CenterAlign}{BarcodeNumbers}{BarcodeHeight1}{BarcodeWidth}{PrintBarcode}<TRANSACTIONNUMBER>/<SEQUENCENUMBER>{Null};
;{CRLF}{CenterAlign}{SmallFont}{Font0}* Price includes GST ;
;{CRLF}{CenterAlign}{SmallFont}{Font0}* This is not a TAX invoice ;
{CRLF}{CenterAlign}{SmallFont}{Font0}Purchase of this tickets indicates ;
{CRLF}{CenterAlign}{SmallFont}{Font0}acceptance of our Terms and Conditions on;
{CRLF}{CenterAlign}{SmallFont}{Font0}display at the ticket counter;
{CRLF}{SmallFont}{Font0};
;******************************************
{FF};
{FF};
{FF};
{PrintBuffer};
{CutPaper};



;-------------------------------------------------------------------
[Footer]
;-------------------------------------------------------------------



;{CRLF}{LeftAlign}{BigFont-2H}{Font5}{ReverseOn}<SessionDayOfWeek> <SESSIONDATE>-<SessionTime>{ReverseOff} {Font5}<FilmTitleFull>(<CENSORRATING>);
;{CRLF}{LeftAlign}{BigFont-2H}{ReverseOn}{Font1}<AREA> <RowID><SeatID>{ReverseOff};  
;{CRLF}{LeftAlign}{SmallFont}{Font0}<TicketPayment> <TransactionNumber>/<SequenceNumber> <TextTax> <TaxNumber>                 ;    {SmallFont}{Font0}Nett : Rs.<PRICEBEFORETAX>;
;{CRLF}{RightAlign}{SmallFont}{Font0}E.Tax: Rs.<PRICETAX1>;
;{CRLF}{RightAlign}{SmallFont}{Font0}T.M.C: Rs.<PRICETAX2>;
;{CRLF}{RightAlign}{SmallFont}{Font0}Total: Rs.<TicketCost>;
;{CRLF}{CenterAlign}{BigFont}{ReverseOn}{Font1}Screen - <ScreenName>{ReverseOff};






;{CRLF} <ADMITDETAILS>;
;{CRLF}<ADMITDETAILSALT>;
;{CRLF}<AREA>{CRLF};
;{CRLF}<AREAALT>{CRLF};
;{CRLF}<AREACATEGORYCODE>;
;{CRLF}<AREACATEGORY>;
;{CRLF}<AREACATEGORYALT>;
;{CRLF}<BARCODE>;
;{CRLF}<CENSORRATING>;
;{CRLF}<CENSORRATINGALT>;
;{CRLF}<CINADDR1>;
;{CRLF}<CINADDR1ALT>;
;{CRLF}<CINADDR2>;
;{CRLF}<CINADDR2ALT>;
;{CRLF}<CINADDR3>;
;{CRLF}<CINADDR3ALT>;
;{CRLF}<CINADDR4>;
;{CRLF}<CINADDR4ALT>;
;{CRLF}<CINEMA>;
;{CRLF}<CINEMAALT>;
;{CRLF}<CINLOCATIONCODE>;
;{CRLF}<CREDITTEXT>;
;{CRLF}<CUSTTICKETREF>;
;{CRLF}<FILMCODE>;
;{CRLF}<FILMMESSAGE1>;
;{CRLF}<FILMMESSAGE2>;
;{CRLF}<FILMMESSAGE3>;
;{CRLF}<FILMMESSAGE4>;
;{CRLF}<FILMTITLEFULL>;
;{CRLF}<FILMTITLEFULLALT>;
;{CRLF}<FILMTITLESHORT>;
;{CRLF}<FILMTITLESHORTALT>;
;{CRLF}<MESSAGE1>;
;{CRLF}<MESSAGE1ALT>;
;{CRLF}<MESSAGE2>;
;{CRLF}<MESSAGE2ALT>;
;{CRLF}<MESSAGESTUB1>;
;{CRLF}<MESSAGESTUB1ALT>;
;{CRLF}<MESSAGESTUB2>;
;{CRLF}<MESSAGESTUB2ALT>;
;{CRLF}<PACKAGEDESC>;
;{CRLF}<PACKAGEDESCALT>;
;{CRLF}<PACKAGECOST>;
;{CRLF}<POSLOCATIONCODE>;
;{CRLF}<PRICEBEFORETAX>;
;{CRLF}<PRICETAX1>;
;{CRLF}<PRICETAX2>;
;{CRLF}<PRICETAX3>;
;{CRLF}<PRICETAX4>;
;{CRLF}<ROWID>;
;{CRLF}<SEATID>;
;{CRLF}<SCREENNAME>;
;{CRLF}<SCREENNAMEALT>;
;{CRLF}<SCREENNO>;
;{CRLF}<SEQUENCENUMBER>;
;{CRLF}<SESSIONDATE>;
;{CRLF}<SESSIONDAY>;
;{CRLF}<SESSIONDAYOFWEEK>;
;{CRLF}<SESSIONEND>;
;{CRLF}<SESSIONSTARTFULL>;
;{CRLF}<SESSIONSTARTSHORT>;
;{CRLF}<SESSIONTIME>;
;{CRLF}<SYSTEMDATE>;
;{CRLF}<SYSTEMTIME>;
;{CRLF}<TAXNUMBER>;
;{CRLF}<TEXTTAX>;
;{CRLF}<TICKETSOURCE>;
;{CRLF}<TICKETTYPECODE>;
;{CRLF}<TICKETTYPECODEALT>;
;{CRLF}<TICKETCOST>;
;{CRLF}<TICKETCOSTCASH2>;
;{CRLF}<TICKETNUMBER>;
;{CRLF}<TICKETNUMBERREFUNDED>;
;{CRLF}<TICKETPAYMENT>;
;{CRLF}<TICKETTYPEDESC>;
;{CRLF}<TICKETTYPEDESCALT>;
;{CRLF}<TIMEZONE>;
;{CRLF}<TRANSACTIONIDENTIFIER>;
;{CRLF}<TRANSACTIONNUMBER>;
;{CRLF}<USER>;
;{CRLF}<USEREGISTRATION>;
;{CRLF}<WORKSTATION>;






;valid fields are:
;-ADMITDETAILS- = <ADMITDETAILS>{CRLF};
;-ADMITDETAILSALT- = <ADMITDETAILSALT>{CRLF};
;-AREA- = <AREA>{CRLF};
;-AREAALT- = <AREAALT>{CRLF};
;-AREACATEGORYCODE- = <AREACATEGORYCODE>{CRLF};
;-AREACATEGORY- = <AREACATEGORY>{CRLF};
;-AREACATEGORYALT- = <AREACATEGORYALT>{CRLF};
;-BARCODE- = <BARCODE>{CRLF};
;-CENSORRATING- = <CENSORRATING>{CRLF};
;-CENSORRATINGALT- = <CENSORRATINGALT>{CRLF};
;-CINADDR1- = <CINADDR1>{CRLF};
;-CINADDR1ALT- = <CINADDR1ALT>{CRLF};
;-CINADDR2- = <CINADDR2>{CRLF};
;-CINADDR2ALT- = <CINADDR2ALT>{CRLF};
;-CINADDR3- = <CINADDR3>{CRLF};
;-CINADDR3ALT- = <CINADDR3ALT>{CRLF};
;-CINADDR4- = <CINADDR4>{CRLF};
;-CINADDR4ALT- = <CINADDR4ALT>{CRLF};
;-CINEMA- = <CINEMA>{CRLF};
;-CINEMAALT- = <CINEMAALT>{CRLF};
;-CINLOCATIONCODE- = <CINLOCATIONCODE>{CRLF};
;-CREDITTEXT- = <CREDITTEXT>{CRLF};
;-CUSTTICKETREF- = <CUSTTICKETREF>{CRLF};
;-FILMCODE- = <FILMCODE>{CRLF};
;-FILMMESSAGE1- = <FILMMESSAGE1>{CRLF};
;-FILMMESSAGE2- = <FILMMESSAGE2>{CRLF};
;-FILMMESSAGE3- = <FILMMESSAGE3>{CRLF};
;-FILMMESSAGE4- = <FILMMESSAGE4>{CRLF};
;-FILMTITLEFULL- = <FILMTITLEFULL>{CRLF};
;-FILMTITLEFULLALT- = <FILMTITLEFULLALT>{CRLF};
;-FILMTITLESHORT- = <FILMTITLESHORT>{CRLF};
;-FILMTITLESHORTALT- = <FILMTITLESHORTALT>{CRLF};
;-MESSAGE1- = <MESSAGE1>{CRLF};
;-MESSAGE1ALT- = <MESSAGE1ALT>{CRLF};
;-MESSAGE2- = <MESSAGE2>{CRLF};
;-MESSAGE2ALT- = <MESSAGE2ALT>{CRLF};
;-MESSAGESTUB1- = <MESSAGESTUB1>{CRLF};
;-MESSAGESTUB1ALT- = <MESSAGESTUB1ALT>{CRLF};
;-MESSAGESTUB2- = <MESSAGESTUB2>{CRLF};
;-MESSAGESTUB2ALT- = <MESSAGESTUB2ALT>{CRLF};
;-PACKAGEDESC- = <PACKAGEDESC>{CRLF};
;-PACKAGEDESCALT- = <PACKAGEDESCALT>{CRLF};
;-PACKAGECOST- = <PACKAGECOST>{CRLF};
;-POSLOCATIONCODE- = <POSLOCATIONCODE>{CRLF};
;-PRICEBEFORETAX- = <PRICEBEFORETAX>{CRLF};
;-PRICETAX1- = <PRICETAX1>{CRLF};
;-PRICETAX2- = <PRICETAX2>{CRLF};
;-PRICETAX3- = <PRICETAX3>{CRLF};
;-PRICETAX4- = <PRICETAX4>{CRLF};
;-ROWID- = <ROWID>{CRLF};
;-SEATID- = <SEATID>{CRLF};
;-SCREENNAME- = <SCREENNAME>{CRLF};
;-SCREENNAMEALT- = <SCREENNAMEALT>{CRLF};
;-SCREENNO- = <SCREENNO>{CRLF};
;-SEQUENCENUMBER- = <SEQUENCENUMBER>{CRLF};
;-SESSIONDATE- = <SESSIONDATE>{CRLF};
;-SESSIONDAY- = <SESSIONDAY>{CRLF};
;-SESSIONDAYOFWEEK- = <SESSIONDAYOFWEEK>{CRLF};
;-SESSIONEND- = <SESSIONEND>{CRLF};
;-SESSIONSTARTFULL- = <SESSIONSTARTFULL>{CRLF};
;-SESSIONSTARTSHORT- = <SESSIONSTARTSHORT>{CRLF};
;-SESSIONTIME- = <SESSIONTIME>{CRLF};
;-SYSTEMDATE- = <SYSTEMDATE>{CRLF};
;-SYSTEMTIME- = <SYSTEMTIME>{CRLF};
;-TAXNUMBER- = <TAXNUMBER>{CRLF};
;-TEXTTAX- = <TEXTTAX>{CRLF};
;-TICKETSOURCE- = <TICKETSOURCE>{CRLF};
;-TICKETTYPECODE- = <TICKETTYPECODE>{CRLF};
;-TICKETTYPECODEALT- = <TICKETTYPECODEALT>{CRLF};
;-TICKETCOST- = <TICKETCOST>{CRLF};
;-TICKETCOSTCASH2- = <TICKETCOSTCASH2>{CRLF};
;-TICKETNUMBER- = <TICKETNUMBER>{CRLF};
;-TICKETNUMBERREFUNDED- = <TICKETNUMBERREFUNDED>{CRLF};
;-TICKETPAYMENT- = <TICKETPAYMENT>{CRLF};
;-TICKETTYPEDESC- = <TICKETTYPEDESC>{CRLF};
;-TICKETTYPEDESCALT- = <TICKETTYPEDESCALT>{CRLF};
;-TIMEZONE- = <TIMEZONE>{CRLF};
;-TRANSACTIONIDENTIFIER- = <TRANSACTIONIDENTIFIER>{CRLF};
;-TRANSACTIONNUMBER- = <TRANSACTIONNUMBER>{CRLF};
;-USER- = <USER>{CRLF};
;-USEREGISTRATION- = <USEREGISTRATION>{CRLF};
;-WORKSTATION- = <WORKSTATION>{CRLF};
;{CRLF};
;{PrintBuffer};
;{FF};
;{CutPaper};
