; TICKET PRINT LAYOUT TEMPLATE				Ticket.txt
; Do NOT amend unless you know what you are doing
; -----------------------------------------------
; Template for Indian Cinema Ticket on Datamax E4203 Printer
; ===============================================

; [Definitions]	section holds substitution string definitions 
;			- helps interpretation of ticket layout/print instructions
; [Header] 	section is sent to printer once before every print job of N tickets
;			- typically may hold printer initialise commands
; [Detail]	section is sent to printer for every one of N tickets
; [Footer]	section is sent to printer once at the end of every print of N tickets
;			- typically may hold printer terminate commands
; The controlling program determines when each section is sent
; The above behave like the corresponding bands in a report writer

; Blank lines ignored
; Anything after ; is a comment  (except in definition lines a ";" is a printercode)
; All lines of info (up to ;) have trailing spaces trimmed

; {Textxxx} normally indicates 'fixed' text which remains constant for all 
;	tickets, independent of database data.  May be a foreign language.
;	In this situation, can hardcode the actual text in this ticket file 
;	rather than set it in the controlling program (if you  want)!


; TRICKS:
; Implementing conditionals, such as
;	if data is blank, no box else a box.
; -------------------------------------------
[Definitions]
; string substitutions for all { } readable-name items are defined below... chars AFTER the =
; Leave NO space after = on [definition] lines
 
;		FieldSeparator

{FB}=f312
{Edge}=R
{L}=L
{D11}=D11
{H14}=H10
{C0020}=C0017
{R0020}=R0020

{INV}=A5
{NOR}=A1
{A}=491100500200005A
;{CINEMA}=492100400600005
{CINEMA}=492100400400005
;{CINEMA}=492100300600005
;{SCREENNO}=49210030600015
;{FILMTITLES}=491100300400020
{FILMTITLES}=491100400400030
;{DATE1}=491100400400040
{DATE1}=491100400300055
{TIME1}=492100300300070
;{TIME1}=492100300400050
;{BOX1}=1X1100000400051B039120001001
{CAT1}=491200500230115
{SER1}=491100200400125
;{SERIAL1}=491100402250000
{SERIAL1}=491100402200110
{SEAT1}=491100402350016
{ROW1}=491100402500030
{CC1}=491100201800022
{CP1}=491100202400021
{TAXF1}= 492100201800035
{COMP1}= 492100201800035
{DP1}= 492100201800035
{COST1}= 491100201700050Nett :         Rs.
{COSTN1}=491100201700060E.Tax:         Rs.
{COSTE1}=491100101800070T.M.C:         Rs.
;{COSTN1}=491100402200060GROSS
;{COSTE1}=491100402200075TICKET
{COSTS1}=491100201700070TOTAL: 
;{NAME1}=491100201700095
;{MESG3}=391100500200095ACCOUNTS


{D}=491100500200170D
;{THEATER3}=492100300400265
{THEATER3}=492100400400170
;{SCREENNO}=492100300600275
;{TITLE3}=491100300400280
{TITLE3}=491100400400190
;{DATE3}=491100400400297
;{DATE3}=491100400400292
{DATE3}=491100400400210
;{TIME3}=492100300400310
{TIME3}=492100300400230
;{BOX3}=1X1100000400311B039120001001
{CAT3}=491200400420280
;{SER3}=491100200400310
{SER3}=491100200400380
;{SERIAL3}=491100402250260
{SERIAL3}=491100401700290
{SEAT3}=4911004030502760
{CC3}=491100201800282
{CP3}=491100202400281
{ROW3}=491100403200290
{TAXF3}=492100201800230
{COMP3}=492100201800230
{COST3}= 491100201700250Nett :         Rs. 
{COSTN3}=491100201700260E.Tax:         Rs.
{COSTE3}=491100101800330T.M.C:         Rs.
;{COSTN3}=491100402200320GROSS
;{COSTE3}=491100402200335TICKET
{COSTS3}=491100201700270TOTAL:
;{NAME3}=491100201800355
;{MESG1}=391100500200350DOORMAN
{Barcode}=4a9400000200350



{QTY}=Q0001
{E}=E
{BR}=013




[Header]
; -------------------------------------------
[Detail]

{BR}{FB};
{BR}{Edge};
{BR}{L};
{BR}{D11};
{BR}{H14};
{BR}{C0020};
{BR}{R0020};
{BR}{A};
{BR}{INV};
{BR}{CINEMA}<SCREENNAME>;
;{BR}{SCREENNAME}<SCREENNAME><SCREENNO>;
{BR}{NOR};
{BR}{FILMTITLES}<FilmTitleShort> (<CensorRating>);
{BR}{DATE1}<SessionDayOfWeek> <SessionDate>;
{BR}{TIME1}<SessionTime>;
;{BR}{BOX1};
{BR}{CAT1}<Area>   <RowId>-<SeatId>;
{BR}{SER1}<TransactionIdentifier>;
{BR}{SERIAL1}<TicketNumber>;
;{BR}{CC1}<PosLocationCode>;
;{BR}{CP1}CMSD-CK(1-5);
{BR}{TAXF1}<IIF~PriceTax1~eq~0.00~TAX FREE~ >;
{BR}{COMP1}<IIF~PriceBeforeTax~eq~0.00~COMP.~ >;
{BR}{DP1}<IIF~TicketTypeDesc~eq~BD~DP.~ >;
{BR}{COST1}<PriceBeforeTax>;
{BR}{COSTN1}<PriceTax1>;
{BR}{COSTE1}<PriceTax2>;
{BR}{COSTS1}<TicketCost>;
;{BR}{NAME1}<CustTicketRef>;
;{BR}{MESG3};

{BR}{D};
{BR}{INV};
{BR}{THEATER3}<SCREENNAME>;
{BR}{NOR};
{BR}{TITLE3}<FilmTitleShort> (<CensorRating>);
{BR}{DATE3}<SessionDayOfWeek> <SessionDate>;
{BR}{TIME3}<SessionTime>;
;{BR}{BOX3};
{BR}{CAT3}<Area>   <RowId>-<SeatId>;
{BR}{SER3}<TransactionIdentifier>;
{BR}{Barcode}<TransactionNumber>;
;{BR}{SER3}<TransactionNumber>;
{BR}{SERIAL3}<TicketNumber>;
;{BR}{Barcode}<TicketNumber>;
;{BR}{CC3}<PosLocationCode>;
;{BR}{CP3}CMSD-CK1(1-5);
{BR}{TAXF3}<IIF~PriceTax1~eq~0.00~TAX FREE~ >;
{BR}{COMP3}<IIF~PriceBeforeTax~eq~0.00~COMP.~ >;
{BR}{COST3}<PriceBeforeTax>;
{BR}{COSTN3}<PriceTax1>;
{BR}{COSTE3}<PriceTax2>;
{BR}{COSTS3}<TicketCost>;
;{BR}{NAME3}<CustTicketRef>;
;{BR}{MESG1};

{BR}{QTY};
{BR}{E};
[Footer]
